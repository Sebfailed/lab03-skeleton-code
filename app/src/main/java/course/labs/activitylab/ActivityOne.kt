package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.View

class ActivityOne : Activity() {

    // lifecycle counts
    //TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called and log the info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        //Log cat print out
        Log.i(TAG, "onCreate called")

        //TODO: update the appropriate count variable & update the view
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    // lifecycle callback overrides

    public override fun onStart() {
        super.onStart()

        //Log cat print out
        Log.i(TAG, "onStart called")
        //TODO:  update the appropriate count variable & update the view
    }

    // TODO: implement 5 missing lifecycle callback methods with Logging and
    // TODO: increment the counter variables' values when their corresponding lifecycle methods are called and log the info

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        //TODO:  save state information with a collection of key-value pairs & save all  count variables
    }


    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityOne"
    }


}
